// Universita' degli Studi di Brescia - Facolta' di Ingegneria
// Corso di laurea magistrale in Ingegneria Informatica
//  A.A. 2011/2012
//
// Progetto per i corsi di Robotica e Digital Image Processing
//  Michael Caldera, Fabio Filisetti
//

#include <iostream>
#include <stdlib.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;

/* ------------------------------------------------------ */

#define BACKGROUND_FILE "/Users/michael/Desktop/video/background.jpg" // file usato come background
#define OUTPUT_DIRECTORY "/Users/michael/Desktop/robotica/img" // directory ove salvare le immagini in output

#define JPEG_QUALITY 30 // da 0 a 100 rappresenta la compressione jpeg

// intervallo percentuale di pixel variati entro il quale viene identificato un movimento
#define PIXEL_CHANGED_MIN_PERC 0.01 // filtra i movimenti minimi ininfluenti 1%
#define PIXEL_CHANGED_MAX_PERC 0.20 // filtra i movimenti dell'intera struttura 20%

#define BACKGROUND_SUBTRACTION_THRESHOLD 60 // soglia per dividere background da foreground da 0 a 255

int output_widths[] = { 500, 400, 300 }; // larghezze delle immagini in output (possono essere aggiunte/rimosse)


/* ------------------------------------------------------ */


// definizione della struct rappresentante la webcam
typedef struct scam
{
    CvCapture* capture; // risorsa della webcam
    
    IplImage* prevFrame; // frame precedente, quello analizzato nel ciclo precedente
    IplImage* prevFrame_GRAY; // frame precedente in scala di grigi
    
    IplImage* currentFrame; // frame corrente appena letto
    IplImage* currentFrame_GRAY; // frame precedente in scala di grigi
    
    IplImage* background; // immagine da usare come sfondo
    
    IplImage* output; // frame da visualizzare in output
    
    IplImage* section; // sezione rettangolare di dimensioni dinamiche (la più piccola possibile) che contiene il robot identificato
    int section_offset_x; // offset della sezione rispetto al frame originale
    int section_offset_y;
    
    int x; // posizionamento corrente del robot
    int y;
    int theta;
} Cam;

typedef Cam *Webcam;

/* ------------------------------------------------------ */

// prototipi delle funzioni

Webcam init();
void end(Webcam w);
IplImage* getFrame(Webcam w);
void nextFrame(Webcam w);
void pickRobotSection(Webcam w);
IplImage* backgroundSubtraction(IplImage* prevFrame_GRAY, IplImage* currentFrame_GRAY);
void drawOutput(Webcam w);
void saveFrames(Webcam w);

/* ------------------------------------------------------ */

int main(int argc, const char * argv[])
{
    Webcam w = init();
    
    cvNamedWindow("w->output", 1);
    cvNamedWindow("w->currentFrame", 1);
    cvNamedWindow("w->prevFrame", 1);
    cvNamedWindow("w->section", 1);
    
    while(1)
    {
        // analizzo il frame successivo
        nextFrame(w);
        
        //saveFrames(w);
        
        IplImage* img = cvCreateImage(cvSize(w->output->width/2, w->output->height/2), 8, 3);
        IplImage* img1 = cvCreateImage(cvSize(w->output->width/2, w->output->height/2), 8, 1);
        
        
        cvResize(w->currentFrame_GRAY, img1);
        cvShowImage("w->currentFrame", img1);
        
        cvResize(w->prevFrame_GRAY, img1);
        cvShowImage("w->prevFrame", img1);
        
        cvShowImage("w->section", w->section);
        
        cvResize(w->output, img);
        cvShowImage("w->output", img);
        
        
        cvWaitKey(0);
        
        if((cvWaitKey(10) & 255)==27) {
            break;
        }
    }
    
    end(w);
    
    return 0;
}

/* ------------------------------------------------------ */
/* ------------------------------------------------------ */
/* ------------------------------------------------------ */

// legge un frame

IplImage* getFrame(Webcam w)
{
    cvGrabFrame(w->capture);
    
    IplImage *frame = cvQueryFrame(w->capture);
    
    if(!frame)
    {
        printf("Errore noframe");
        exit(-2);
    }
    
    return frame;
}

/* ------------------------------------------------------ */

// inizializza la struct
Webcam init()
{
    Webcam w;
    w = (Webcam)malloc(sizeof(Cam));
    
    w->capture = cvCaptureFromCAM(0);
//    w->capture = cvCaptureFromAVI("/Users/michael/Desktop/video/robot2.avi");
    
    if(!w->capture)
    {
        printf("Non posso inizializzare la webcam\n");
        exit(-1);
    }
    
    w->prevFrame = getFrame(w);
    w->prevFrame_GRAY = cvCreateImage(cvGetSize(w->prevFrame), 8, 1);
    
    w->currentFrame = getFrame(w);
    w->currentFrame_GRAY = cvCreateImage(cvGetSize(w->currentFrame), 8, 1);
    
    w->section = cvCreateImage(cvGetSize(w->currentFrame), 8, 3);
    
    // carico l'immagine di sfondo da file esterno e la ridimensiono come il frame
    IplImage* bg = cvLoadImage(BACKGROUND_FILE, 1);
    
    if(bg == NULL)
    {
        printf("Background file not found");
        exit(0);
    }
    
    w->background = cvCreateImage(cvGetSize(w->currentFrame), 8, 3);
    cvResize(bg, w->background);
    cvReleaseImage(&bg);
    
    w->output = cvCreateImage(cvGetSize(w->currentFrame), 8, 3);
    cvCopy(w->background, w->output);
    
    nextFrame(w);
    
    return w;
}

/* ------------------------------------------------------ */

void end(Webcam w)
{
    cvReleaseCapture(&w->capture);
}

/* ------------------------------------------------------ */

// analizza il prossimo frame

void nextFrame(Webcam w)
{
    // shifto il vecchio frame corrente come il precedente 
    cvCopy(w->currentFrame, w->prevFrame, 0);
    cvCopy(w->currentFrame_GRAY, w->prevFrame_GRAY, 0);
    
    // acquisisco il nuovo frame
    w->currentFrame = getFrame(w);
    cvCvtColor(w->currentFrame, w->currentFrame_GRAY, CV_BGR2GRAY);
    
    // applico il filtro gaussiano al frame corrente
    cvSmooth(w->currentFrame, w->currentFrame, CV_GAUSSIAN, 3, 0, 0, 0);
    
    // trovo la sezione rettangolare di dimensioni dinamiche che contiene il robot identificato
    pickRobotSection(w);
    
    return;
}

/* ------------------------------------------------------ */

// trova la sezione rettangolare di dimensioni dinamiche (la più piccola possibile) che contiene il robot identificato

void pickRobotSection(Webcam w)
{
    // effettuo la sottrazione del background
    IplImage* background = backgroundSubtraction(w->prevFrame_GRAY, w->currentFrame_GRAY);
    
    cvNamedWindow("backgroundSubtraction()", 1);
    IplImage* img = cvCreateImage(cvSize(w->prevFrame->width/2, w->prevFrame->height/2), 8, 1);
    cvResize(background, img);
    cvShowImage("backgroundSubtraction()", img);
    
    
    
    // rappresenta il numero di pixel che sono variati, cioè soggetti a spostamento
    int changedPixelsNum = 0;
    
    // inizializzo i 4 punti che mi permettono di identificare la sezione
    int i_min = background->width, i_max = 0, j_min = background->height, j_max = 0;
    
    for(int i=0; i<background->height ; i++)
    {
        for(int j=0 ; j<background->width ; j++)
        {
            if(cvGet2D(background, i, j).val[0] == 255) // se il pixel è stato soggetto ad una variazione
            {
                changedPixelsNum++;
                
                if(i < i_min) { i_min = i; }
                else if(i > i_max) { i_max = i; }
                
                if(j < j_min) { j_min = j; }
                else if(j > j_max) { j_max = j; }
            }
        }
    }
    
    // verifico che changedPixelsNum sia tra il un certo intervallo percentuale della dimensione dell'immagine
    
    float min = background->width * background->height * PIXEL_CHANGED_MIN_PERC;
    float max = background->width * background->height * PIXEL_CHANGED_MAX_PERC;
    
    // se i pixel variati sono significativi
    if(changedPixelsNum < max && changedPixelsNum > min && i_min < i_max && j_min < j_max)
    {
        // creo l'immagine che conterrà il robot
        cvReleaseImage(&w->section);
        w->section = cvCreateImage(cvSize(j_max-j_min, i_max-i_min), 8, 3);
        
        // coloro i pixel della sezione
        for(int i=i_min ; i<i_max ; i++)
        {
            for(int j=j_min ; j<j_max ; j++) {
                cvSet2D(w->section, i-i_min, j-j_min, cvGet2D(w->currentFrame, i, j));
            }
        }
        
        // salvo gli offset
        w->section_offset_x = j_min;
        w->section_offset_y = i_min;
        
        drawOutput(w);
    }
    else
    {
        w->section_offset_x = 0;
        w->section_offset_y = 0;
    }
    
    cvReleaseImage(&background);
}

/* ------------------------------------------------------ */

// effettua la sottrazione del background

IplImage* backgroundSubtraction(IplImage* prevFrame_GRAY, IplImage* currentFrame_GRAY)
{
    IplImage* background = cvCreateImage(cvGetSize(currentFrame_GRAY), 8, 1);
    
    cvAbsDiff(currentFrame_GRAY, prevFrame_GRAY, background);
    
    cvThreshold(background, background, BACKGROUND_SUBTRACTION_THRESHOLD, 255, CV_THRESH_BINARY);
    cvErode(background, background, 0, 1);
    cvDilate(background, background, 0, 9);
    cvDilate(background, background, 0, 10);
    cvErode(background, background, 0, 1);
    
    return background;
}

/* ------------------------------------------------------ */

// crea l'immagine da spedire in output

void drawOutput(Webcam w)
{
    cvCopy(w->background, w->output);
    
    // converto la sezione in scala di grigi
    IplImage *temp2 = cvCreateImage(cvGetSize(w->section), 8, 1);
    cvCvtColor(w->section, temp2, CV_BGR2GRAY);
    
    // equalizzo
    cvEqualizeHist(temp2, temp2);
    
    cvThreshold(temp2, temp2, BACKGROUND_SUBTRACTION_THRESHOLD, 255, CV_THRESH_BINARY);
    
    // identifico solo i pixel del robot e li disegno sull'immagine output
    for(int i=0; i<temp2->height ; i++)
    {
        for(int j=0 ; j<temp2->width ; j++)
        {
            if(cvGet2D(temp2, i, j).val[0] != 255)
            {
                int x = j+w->section_offset_x;
                int y = i+w->section_offset_y;
                cvSet2D(w->output, y, x, cvGet2D(w->currentFrame, y, x));
            }
        }
    }
    
    cvReleaseImage(&temp2);
}

/* ------------------------------------------------------ */

// salvo le immagini necessarie all'output
void saveFrames(Webcam w)
{
    // parametri di salvataggio
    int p[] = { CV_IMWRITE_JPEG_QUALITY, JPEG_QUALITY, 0 };
    
    // supporto per il ridimensionamento
    int width, height;
    char name[200];
    IplImage *temp;
    
    for(int i=0 ; i<sizeof(output_widths)/sizeof(int) ; i++)
    {
        width = output_widths[i];
        height = width * w->currentFrame->height / w->currentFrame->width;
        
        temp = cvCreateImage(cvSize(width, height), 8, 3);
        
        sprintf(name, "%s/original_%i.jpg", OUTPUT_DIRECTORY, width);
        cvResize(w->currentFrame, temp);
        cvSaveImage(name, temp, p);
        
        
        sprintf(name, "%s/output_%i.jpg", OUTPUT_DIRECTORY, width);
        cvResize(w->output, temp);
        cvSaveImage(name, temp, p);
        
        cvReleaseImage(&temp);
    }
}